package JuegoAhorcado;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;


public class JuegoAhorcado extends javax.swing.JFrame {

	//imagenes
	
	String imagenes[] = {
			
		"C:\\juego_ahorcado\\foto1.png",
		"C:\\juego_ahorcado\\foto2.png",
		"C:\\juego_ahorcado\\foto3.png",
		"C:\\juego_ahorcado\\foto4.png",
		"C:\\juego_ahorcado\\foto5.png",
		"C:\\juego_ahorcado\\foto6.png",
		"C:\\juego_ahorcado\\foto7.png",
		"C:\\juego_ahorcado\\FOTO8.png"
		
	};
	
	int indiceImagen = 0;
	
	//PROGRAMAMOS LA PALABRA SECRETA
	
	String [] SecretP = {"comida","dinosaurio","televisor","shushi","conejo","electrodomestico","array","boolean","programacion","tecnologia"};
	
	//creamos posicion aleatoria
	int posicionAleatoria = (int) Math.floor(Math.random() * SecretP.length);
	String palabra = SecretP[posicionAleatoria];
	
	//convertimos esa palabra en un array de letras
	char [] letras = palabra.toCharArray();
	char []letrasconGuiones= new char [letras.length];
	
	
	
	

	
	
	private JPanel contentPane;
	private JTextField textPalabra;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JuegoAhorcado frame = new JuegoAhorcado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JuegoAhorcado() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 804, 597);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btn1 = new JButton("A");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'A';
				
				recorreParaula(L);
				
				
			}
		});
		btn1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn1.setBounds(20, 261, 84, 33);
		contentPane.add(btn1);
		
		JButton btn5 = new JButton("F");
		btn5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'F';
				
				recorreParaula(L);
			}
		});
		btn5.setBounds(20, 293, 84, 33);
		contentPane.add(btn5);
		
		JButton btn9 = new JButton("K");
		btn9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'K';
				
				recorreParaula(L);
				
			}
		});
		btn9.setBounds(20, 325, 84, 33);
		contentPane.add(btn9);
		
		JButton btn13 = new JButton("O");
		btn13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'O';
				
				recorreParaula(L);
				
		
			}
		});
		btn13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn13.setBounds(20, 352, 84, 33);
		contentPane.add(btn13);
		
		JButton btn17 = new JButton("T");
		btn17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'T';
				
				recorreParaula(L);
			}
		});
		btn17.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn17.setBounds(20, 383, 84, 33);
		contentPane.add(btn17);
		
		JButton btn21 = new JButton("Y");
		btn21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'Y';
				
				recorreParaula(L);
			}
		});
		btn21.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn21.setBounds(20, 416, 84, 33);
		contentPane.add(btn21);
		
		JButton btn2 = new JButton("B");
		btn2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'B';
				
				recorreParaula(L);
		
			}
		});
		btn2.setBounds(95, 261, 97, 33);
		contentPane.add(btn2);
		
		JButton btn6 = new JButton("G");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char L = 'G';
				
				recorreParaula(L);
				
			
				
				//textPalabra.setText(textPalabra.getText() + "G");
			}
		});
		btn6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn6.setBounds(95, 293, 97, 33);
		contentPane.add(btn6);
		
		JButton btn10 = new JButton("L");
		btn10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'L';
				
				recorreParaula(L);
			}
		});
		btn10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn10.setBounds(95, 325, 97, 33);
		contentPane.add(btn10);
		
		JButton btn14 = new JButton("P");
		btn14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'P';
				
				recorreParaula(L);
				
				
			}
		});
		btn14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn14.setBounds(95, 352, 97, 33);
		contentPane.add(btn14);
		
		JButton btn18 = new JButton("U");
		btn18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				char L = 'U';
				
				recorreParaula(L);

			}
		});
		btn18.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn18.setBounds(95, 383, 97, 33);
		contentPane.add(btn18);
		
		JButton btn22 = new JButton("Z");
		btn22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'Z';
				
				recorreParaula(L);
				
			}
		});
		btn22.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn22.setBounds(95, 416, 97, 33);
		contentPane.add(btn22);
		
		JButton btn3 = new JButton("C");
		btn3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				char L = 'C';
				
				recorreParaula(L);
				
			
				
			}
		});
		btn3.setBounds(189, 261, 97, 33);
		contentPane.add(btn3);
		
		JButton btn7 = new JButton("H");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'H';
				
				recorreParaula(L);
			}
		});
		btn7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn7.setBounds(189, 293, 97, 33);
		contentPane.add(btn7);
		
		JButton btn11 = new JButton("M");
		btn11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				char L = 'M';
				
				recorreParaula(L);
				
			}
		});
		btn11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn11.setBounds(189, 325, 97, 33);
		contentPane.add(btn11);
		
		JButton btn15 = new JButton("Q");
		btn15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'Q';
				
				recorreParaula(L);
				
				
			}
		});
		btn15.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn15.setBounds(189, 352, 97, 33);
		contentPane.add(btn15);
		
		JButton btn19 = new JButton("V");
		btn19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'V';
				
				recorreParaula(L);
			
			}
		});
		btn19.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn19.setBounds(189, 383, 97, 33);
		contentPane.add(btn19);
		
		JButton btn4 = new JButton("D");
		btn4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'D';
				
				recorreParaula(L);
			}
		});
		btn4.setBounds(284, 261, 97, 33);
		contentPane.add(btn4);
		
		JButton btn8 = new JButton("I");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'I';
				
				recorreParaula(L);
			}
		});
		btn8.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn8.setBounds(284, 293, 97, 33);
		contentPane.add(btn8);
		
		JButton btn12 = new JButton("N");
		btn12.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'N';
				
				recorreParaula(L);
			}
		});
		btn12.setBounds(284, 325, 97, 33);
		contentPane.add(btn12);
		
		JButton btn16 = new JButton("R");
		btn16.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btn16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textPalabra.setText(textPalabra.getText() + "R");
			}
		});
		btn16.setBounds(284, 352, 97, 33);
		contentPane.add(btn16);
		
		JButton btn20 = new JButton("W");
		btn20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'W';
				
				recorreParaula(L);
			}
		});
		btn20.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn20.setBounds(284, 383, 97, 33);
		contentPane.add(btn20);
		
		JButton btn4_1 = new JButton("E");
		btn4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'E';
				
				recorreParaula(L);
			}
		});
		btn4_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn4_1.setBounds(380, 261, 97, 33);
		contentPane.add(btn4_1);
		
		JButton btn4_2 = new JButton("J");
		btn4_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'J';
				
				recorreParaula(L);
			}
		});
		btn4_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn4_2.setBounds(380, 293, 97, 33);
		contentPane.add(btn4_2);
		
		JButton btn4_3 = new JButton("Ñ");
		btn4_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'Ñ';
				
				recorreParaula(L);
			}
		});
		btn4_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn4_3.setBounds(380, 325, 97, 33);
		contentPane.add(btn4_3);
		
		JButton btn4_4 = new JButton("S");
		btn4_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'S';
				
				recorreParaula(L);
			}
		});
		btn4_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn4_4.setBounds(380, 352, 97, 33);
		contentPane.add(btn4_4);
		
		JButton btn4_5 = new JButton("X");
		btn4_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char L = 'X';
				
				recorreParaula(L);
			}
		});
		btn4_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn4_5.setBounds(380, 383, 97, 33);
		contentPane.add(btn4_5);
		
		textPalabra = new JTextField();
		textPalabra.setBorder(new LineBorder(new Color(171, 173, 179), 2, true));
		textPalabra.setForeground(Color.WHITE);
		textPalabra.setBackground(Color.BLACK);
		textPalabra.setBounds(20, 196, 457, 54);
		contentPane.add(textPalabra);
		textPalabra.setColumns(10);
		
		JLabel secret = new JLabel("Palabra Secreta");
		secret.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		secret.setBounds(20, 159, 293, 20);
		contentPane.add(secret);
		
		JButton btninicio = new JButton("INICIO JUEGO");
		btninicio.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		btninicio.setBackground(new Color(60, 179, 113));
		btninicio.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btninicio.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				//visort_imagenes.setIcoon(imagen);
				
				//indice =0;
				intentosP=0;
				intentosN=0;
				
				for (int i = 0; i <8 ; i++) {
					
			//		if (letras == recorreParaula()) {
						
					}
					
				}
				
				
				/*for (int i=0; i<letras.length;i++) {
					//aqui convertimos todas las letras de la palabra en guiones bajos
					letrasconGuiones[i]='.';
				}
				
				for(int i = 0; i<10; i++) {
					for(int j = 0; j<10; j++) {
					
					add(btn1);
					add(btn2);
					add(btn3);
					add(btn4);
					add(btn5);
					add(btn6);
					add(btn7);
					add(btn8);
					add(btn9);
					add(btn10);
					add(btn11);
					add(btn12);
					add(btn13);
					add(btn14);
					add(btn15);
					add(btn16);
					add(btn17);
					add(btn18);
					add(btn19);
					add(btn20);
					add(btn21);
					add(btn22);
					
					
						if (palabra[i] == btn1.getText() || (palabra[i] == btn2.getText()) || (palabra[i] == btn3.getText()) || (palabra[i] == btn4.getText())|| (palabra[i] == btn5.getText()) || (palabra[i] == btn6.getText()) || (palabra[i] == btn7.getText()) || (palabra[i] == btn8.getText()) || (palabra[i] == btn9.getText()) || (palabra[i] == btn10.getText()) || (palabra[i] == btn11.getText()) || (palabra[i] == btn12.getText()) || (palabra[i] == btn13.getText()) || (palabra[i] == btn14.getText()) || (palabra[i] == btn15.getText()) || (palabra[i] == btn16.getText()) || (palabra[i] == btn17.getText())|| (palabra[i] == btn18.getText())|| (palabra[i] == btn19.getText())||(palabra[i] == btn19.getText())|| (palabra[i] == btn20.getText())||(palabra[i] == btn21.getText())||(palabra[i] == btn22.getText())) {
							
							letrasconGuiones[j]= palabra[i];
							
						
						}
						else{
							intentosN++;
						}
					
				//System.out.println("introduce una letra");
				//Scanner lector = new Scanner (System.in);
				/*
				char letraIntroducida = lector.next().charAt(0); //como que el lector no tiene char, pongo que es un string pero como que lo marca erroneo,
				//digo que solo cojo la primera letra
				
				//comprobamos si la letra esta en letras
				
				for (int j=0; j<letras.length; j++) {
					if(letras[i] == letraIntroducida){
						//System.out.println("Exito"); no quiero imprimir exito, quiero imprimir la letra introducida dentro de su posicion mientras las otras siguen de incognito
						letrasconGuiones[j]= letraIntroducida;
									
					}
					
					//si la letra introducida es erronea, se cuenta numero de fallos
					if (letraIntroducida != letras[i]) {
						
						
					}
				}
				
			
				
				if (Arrays.equals(letras,letrasconGuiones)) { //comparamos los dos arrays si la palabra coincide hemos ganado
					System.out.println("Exito");
				}
				//textPalabra.setText(letrasconGuiones);
				
				
			
			
					}
				}*/
			}
		
		
			
		);
		btninicio.setBounds(15, 11, 462, 33);
		contentPane.add(btninicio);
		
		JButton btnResolver = new JButton("Resolver");
		btnResolver.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		btnResolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				//imprimimos la palabra 
				textPalabra.setText(palabra);
				
			}
		});
		btnResolver.setBackground(new Color(244, 164, 96));
		btnResolver.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnResolver.setBounds(15, 55, 462, 33);
		contentPane.add(btnResolver);
		
		JLabel visort_imagenes = new JLabel("");
		visort_imagenes.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		ImageIcon imagen = new ImageIcon("C:\\juego_ahorcado\\foto1.png");
		visort_imagenes.setDisabledIcon(imagen);
		visort_imagenes.setBounds(508, 11, 237, 395);
		contentPane.add(visort_imagenes);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		btnSalir.setFont(new Font("Times New Roman", Font.BOLD, 11));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//programamos el boton para salir
				System.exit(0);
			}
		});
		btnSalir.setBounds(508, 463, 133, 33);
		contentPane.add(btnSalir);
		
	}
	
	public void recorreParaula(char L) {
		recorreParaula(L);
		
		}
	
	int intentosP;
	int intentosN;
}
